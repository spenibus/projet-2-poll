﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webApp.Models;
using webApp.Models.Forms;
using webApp.Toolbox;

namespace webApp.Controllers
{
    public partial class HomeController : MainController
    {
        // GET: Vote
        public ActionResult Json(Int32 id)
        {
            Poll poll = (new DAL()).GetPollById(id);
            return new JsonResult()
            {                
                Data = new
                {
                    id = poll.id
                    , title = poll.title
                    , dateCreation = poll.dateCreation.ToString("o")
                    , isMulti = poll.isMulti
                    , enabled = poll.enabled
                    , voters = poll.voters
                    , answers = poll.answers

                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}