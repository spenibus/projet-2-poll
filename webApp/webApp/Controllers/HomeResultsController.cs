﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webApp.Models;

namespace webApp.Controllers
{
    public partial class HomeController : MainController
    {
        // GET: Results
        public ActionResult Results(Int32 id)
        {
            // get poll data
            Poll poll = (new DAL()).GetPollById(id);

            // poll does not exist
            if (poll.id == 0)
            {
                LogError("Poll does not exist");
                return View("Error");
            }
            return View(poll);
        }
    }
}