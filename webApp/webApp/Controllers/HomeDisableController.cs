﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webApp.Models;
using webApp.Models.Forms;
using webApp.Toolbox;

namespace webApp.Controllers
{
    public partial class HomeController : MainController
    {
        // GET: Disable
        public ActionResult Disable(Int32 id, string token)
        {
            Poll poll = (new DAL()).GetPollById(id);

            // poll does not exist
            if (poll.id == 0)
            {
                LogError("Poll does not exist");
                return View("Error");
            }
            // poll already disabled
            else if (!poll.enabled)
            {
                LogNotice("Poll has already been disabled"
                    + "<br/><a href=\"" + Tools.UrlAction("Results", poll.id) + "\">View results</a>");
                return View("Error");
            }
            // invalid token
            else if (poll.token != token)
            {
                LogError("Security token is invalid");
                return View("Error");
            }
            
            return View(poll);
        }

        // GET: DisableSubmit
        public ActionResult DisableSubmit(PollFormDisable pForm)
        {

            Poll poll = (new DAL()).GetPollById(pForm.id);

            // poll does not exist
            if (poll.id == 0)
            {
                LogError("Poll does not exist");
                return View("Error");
            }
            // poll already disabled
            else if (!poll.enabled)
            {
                LogNotice("Poll has already been disabled"
                    + "<br/><a href=\"" + Tools.UrlAction("Results", poll.id) + "\">View results</a>");
                return View("Error");
            }
            // invalid token
            else if (poll.token != pForm.token)
            {
                LogError("Security token is invalid");
                return View("Error");
            }

            // proceed
            poll.Disable();

            return RedirectToAction("Results", new { id = poll.id });
        }
    }
}