﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webApp.Toolbox;

namespace webApp.Controllers
{
    public class MainController : Controller
    {
        protected List<string> mainLogError   = new List<string>();
        protected List<string> mainLogWarning = new List<string>();
        protected List<string> mainLogNotice  = new List<string>();

        // runs before all others controllers
        public MainController()
        {
            // reference the logs in the view
            ViewBag.logError   = this.mainLogError;
            ViewBag.logWarning = this.mainLogWarning;
            ViewBag.logNotice  = this.mainLogNotice;
        }

        public void LogError(string msg)
        {
            this.mainLogError.Add(msg);
        }

        public void LogWarning(string msg)
        {
            this.mainLogWarning.Add(msg);
        }
        public void LogNotice(string msg)
        {
            this.mainLogNotice.Add(msg);
        }
    }
}