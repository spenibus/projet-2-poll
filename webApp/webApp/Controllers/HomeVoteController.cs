﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webApp.Models;
using webApp.Models.Forms;
using webApp.Toolbox;

namespace webApp.Controllers
{
    public partial class HomeController : MainController
    {
        // GET: Vote
        public ActionResult Vote(Int32 id = 0, string token = "")
        {
            DAL dal = new DAL();
            Poll poll = dal.GetPollById(id);
            Client client = new Client();

            // poll does not exist
            if (poll.id == 0)
            {
                LogError("Poll does not exist");
                return View("Error");
            }
            // poll is disabled
            else if (!poll.enabled)
            {
                LogNotice("Poll has been disabled"
                    + "<br/><a href=\"" + Tools.UrlAction("Results", poll.id) + "\">View results</a>");
                return View("Error");
            }
            // client has already voted
            else if (!client.CanVote(poll.id))
            {
                LogNotice("You have already voted in this poll"
                    + "<br/><a href=\"" + Tools.UrlAction("Results", poll.id) + "\">View results</a>");
                return View("Error");
            }

            // everything is fine
            return View(poll);
        }

        // GET: VoteSubmit
        public ActionResult VoteSubmit(PollFormVote pForm)
        {
            DAL dal = new DAL();
            Poll poll = dal.GetPollById(pForm.id);
            Client client = new Client();

            // poll does not exist
            if (poll.id == 0)
            {
                LogError("Poll does not exist");
                return View("Error");
            }
            // poll is disabled
            else if (!poll.enabled)
            {
                LogNotice("Poll has been disabled"
                    + "<br/><a href=\"" + Tools.UrlAction("Results", poll.id) + "\">View results</a>");
                return View("Error");
            }
            // client has already voted
            else if (!client.CanVote(poll.id))
            {
                LogNotice("You have already voted in this poll"
                    + "<br/><a href=\"" + Tools.UrlAction("Results", poll.id) + "\">View results</a>");
                return View("Error");
            }
            // there is not at least one vote
            else if (pForm.votes.Count < 1)
            {
                LogError("You need to choose at least one option");
                return View("Error");
            }

            // cast votes
            poll.CastVotes(pForm.votes);

            // flag the client to avoid multiple voting
            client.HasVoted(poll.id);

            // go to results
            return RedirectToAction("Results", new { id = poll.id });
        }
    }
}