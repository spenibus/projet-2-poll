﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webApp.Models;
using webApp.Models.Forms;
using webApp.Toolbox;

namespace webApp.Controllers
{
    public partial class HomeController : MainController
    {
        // GET: New
        public ActionResult New()
        {
            Poll poll = new Poll();
            return View(poll);
        }

        // GET: NewSubmit
        public ActionResult NewSubmit(PollFormEdit pForm)
        {
            // remove empty answers
            pForm.answers.RemoveAll(item => item == "");

            // check empty title
            if (pForm.title == null)
            {
                LogError("Title can not be empty");
                return View("Error");
            }

            // check there is at least 2 answers
            if (pForm.answers.Count < 2)
            {
                LogError("There must be at least 2 answers");
                return View("Error");
            }

            // get a fresh token
            var token = Tools.GenerateToken();

            // create the poll, get its id
            Int32 id = (new DAL()).CreatePoll(
                pForm.title 
                ,pForm.multi
                ,token
                ,pForm.answers
            );

            // it went well, presumably
            return RedirectToAction("Done", new { id = id, token = token });
        }
    }
}