﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using webApp.Models;
using webApp.Toolbox;

namespace webApp.Controllers
{
    public partial class HomeController : MainController
    {
        // GET: Done
        public ActionResult Done(Int32 id = 0, string token = "")
        {
            List<string> errorMsgs = new List<string>();

            DAL dal = new DAL();

            var poll = dal.GetPollById(id);

            if (poll.id == 0) {
                LogError("poll does not exist");
                return View("Error");
            }
            else if (poll.token != token)
            {
                LogError("invalid token");
                return View("Error");
            }

            // everything is fine
            return View(poll);
        }
    }
}