﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace webApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            /***
            set up custom routes in bulk
            always using controller "Home"
            url format: /{action}/{id}/{token}
            ***/
            foreach (string  actionName in (new string[]{
                "New"
                ,"Done"
                ,"Vote"
                ,"Results"
                ,"Disable"
                ,"Error"
                ,"Json"
            })) {
                routes.MapRoute(
                    name: actionName,
                    url: actionName+"/{id}/{token}",
                    defaults: new {
                        controller = "Home"
                        ,action = actionName
                        ,id = UrlParameter.Optional
                        ,token = UrlParameter.Optional
                    }
                );
            }
            
            // define index route separately because url is empty
            routes.MapRoute(
                name: "Index",
                url: "",
                defaults: new { controller = "Home", action = "Index"}
            );

            // default route
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
