﻿var elemAnswerBox;
var elemAnswer;

function notEnoughAnswers() {
    return $('.dpAnswer').length <= 2
        ? true
        : false;
}


function updateDeleteButtonStatus() {

    var elems = $('.dpAnswer');

    if (notEnoughAnswers()) {
        elems.find('.dpDelete').attr("disabled", true);
    }
    else {
        elems.find('.dpDelete').attr("disabled", false);
    }
}


function insertAnswer() {

    // clone answer model
    var elem = elemAnswer.clone();

    // add deletion handler
    elem.find('.dpDelete').click(deleteAnswer);

    // insert answer
    elemAnswerBox.append(elem);

    updateDeleteButtonStatus();
}


function deleteAnswer(e) {

    console.log('delete');

    // less than 2 answers inputs available
    if (notEnoughAnswers()) {
        // no deletion allowed
        console.log('no delete');
        return;
    }

    // delete answer input
    var z = $(e.target).parents('.dpAnswer').remove();

    updateDeleteButtonStatus();
}


$(document).ready(function () {

    // get answers container
    elemAnswerBox = $('#dpAnswerBox');

    // get answer input model
    elemAnswer = $('.dpAnswer').detach();

    // attach deletion handler to button
    $('#dpAdd').click(insertAnswer);

    // insert 2 inputs by default
    insertAnswer();
    insertAnswer();
});