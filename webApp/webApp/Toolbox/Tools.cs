﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace webApp.Toolbox
{
    public class Tools
    {
       
        public static void DoNothing()
        {
            // meant literally, nothing to see here
        }

        /***
        returns an absolute url
        route values are only added if supplied explicitly
        ***/
        public static string UrlAction(
            string actionName
            , Int32? id = null
            , string token = null
        ){
            // delete route data
            var ctx = HttpContext.Current.Request.RequestContext;
            ctx.RouteData = new System.Web.Routing.RouteData();

            // add id param if supplied
            if (id.HasValue)
            {
                ctx.RouteData.Values.Add("id", id.Value);
            }

            // add token param if supplied
            if (token != null)
            {
                ctx.RouteData.Values.Add("token", token);
            }

            // get basic url
            string url = (new UrlHelper(ctx).Action(
                actionName
                , "Home"
                , null //routeValues
                , HttpContext.Current.Request.Url.Scheme
            ));

            return url;
        }
        
        /***
        returns an alphanumeric string of 64 hex characters
        ***/
        public static string GenerateToken()
        {
            // characters range
            char[] chars = "1234567890ABCDEF".ToCharArray();
            
            // random values
            byte[] rand = new byte[64];
            
            // build token string
            string token = "";
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(rand);
                foreach (byte r in rand)
                {
                    token += chars[r % chars.Length];
                }
            }
            return token;
        }
    }
}