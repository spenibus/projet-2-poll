﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace webApp.Toolbox
{
    public class SqlQuery
    {
        private SqlCommand command;

        /***
        constructor
        set the query string at instantiation, immutable
        ***/
        public SqlQuery(string queryString)
        {
            this.command = new SqlCommand(queryString);
        }

        /***
        add/update parameter
        ***/
        public SqlQuery SetParam(string paramName, object paramValue)
        {
            // update existing param
            if (this.command.Parameters.Contains(paramName))
            {
                this.command.Parameters[paramName].Value = paramValue;
            }
            // add new param
            else {
                this.command.Parameters.AddWithValue(paramName, paramValue);
            }
            // allow chaining
            return this;
        }
        
        /***
        clear all parameters
        ***/
        public SqlQuery ClearParams()
        {
            this.command.Parameters.Clear();
            // allow chaining
            return this;
        }

        /***
        execute the query
        returns the result as a datatable
        ***/
        public DataTable Query()
        {
            DataTable sqlData = new DataTable();

            using (SqlConnection connection = new SqlConnection(
                "Data Source=(LocalDB)\\MSSQLLocalDB;"
                + "AttachDbFilename=" + AppDomain.CurrentDomain.GetData("DataDirectory").ToString() + "\\db.mdf;"
                + "Integrated Security=True"
            ))
            {
                this.command.Connection = connection;
                connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(this.command);
                da.Fill(sqlData);
                da.Dispose();
                this.command.Connection = null;
            }
            return sqlData;
        }

        /***
        execute the query
        ignores the result and returns the instance to make it chainable
        ***/
        public SqlQuery Exec() {
            // run query
            this.Query();
            // allow chaining
            return this;
        }
    }
}