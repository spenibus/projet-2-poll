﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using webApp.Toolbox;
namespace webApp.Models
{
    public class Poll
    {
        
        public Int32 id { get; set; } = 0;
        public string title { get; set; } = ""; // actually the question of the poll
        public DateTime dateCreation { get; set; } = DateTime.Now;
        public bool isMulti { get; set; } = false; // flag for single/multiple answers
        public bool enabled { get; set; } = false;
        public string token { get; set; } = Tools.GenerateToken(); // security token
        public Int32 voters { get; set; } = 0; // numbers of voters
        public List<Answer> answers { get; set; } = new List<Answer>();

        public Poll()
        {
        }

        // cast votes
        public Poll CastVotes(List<Int32> ids)
        {
            // do nothing if poll is a dummy or disabled
            if (this.id == 0 || !this.enabled) {
                return this;
            }

            // keep only votes that belong to this poll
            List<Int32> validVotes = new List<Int32>();
            foreach (Int32 id in ids) {
                if(this.answers.Exists(x => x.id == id)) 
                {
                    validVotes.Add(id);
                }
            }

            // do nothing if there is not at least one vote
            if (validVotes.Count < 1)
            {
                return this;
            }

            // do nothing if poll is single vote but multiple votes have been submitted
            if (!this.isMulti && validVotes.Count > 1) {
                return this;
            }

            // seems valid, go forth
            (new DAL()).CastPollVotes(this.id, validVotes);

            // allow chaining
            return this;
        }
        
        // disable the poll
        public Poll Disable()
        {
            // do nothing if poll is a dummy or disabled
            if (this.id == 0 || !this.enabled)
            {
                return this;
            }

            // seems valid, go forth
            (new DAL()).DisablePollById(this.id);

            // allow chaining
            return this;
        }
    }
}