﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace webApp.Models.Forms
{
    public class PollFormEdit
    {
        public string title { get; set; } = "";
        public bool multi { get; set; } = false;
        public List<string> answers { get; set; } = new List<string>();
    }

    public class PollFormVote
    {
        public Int32 id { get; set; } = 0;
        public List<Int32> votes { get; set; } = new List<Int32>();
    }

    public class PollFormDisable
    {
        public Int32 id { get; set; } = 0;
        public string token { get; set; } = "";
    }
}