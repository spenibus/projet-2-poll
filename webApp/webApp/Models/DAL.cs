﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using webApp.Toolbox;

namespace webApp.Models
{
    /***
    data access layer
    ***/
    public class DAL
    {
        /***
        create a new poll
        ***/
        public Int32 CreatePoll(string title, bool multi, string token, List<string> answersLabels)
        {
            // insert new poll into db, fetch inserted id
            var sqlData = (new SqlQuery(
                "INSERT INTO polls (title, isMulti, token)"
                + " VALUES (@title, @isMulti, @token);"
                + "SELECT SCOPE_IDENTITY();"
            ))
            .SetParam("@title", title)
            .SetParam("@isMulti", multi)
            .SetParam("@token", token)
            .Query();

            // last inserted id (convert because sql returns a decimal)
            Int32 lastId = Convert.ToInt32(sqlData.Rows[0][0]);

            /***
            insert associated answers into db
            lazy, one by one
            http://stackoverflow.com/a/34628542
            ***/

            // prepare query
            var sql = (new SqlQuery(
                "INSERT INTO answers (label, poll_id) VALUES (@label, @poll_id);"
            ))
            .SetParam("@poll_id", lastId);

            // insert each answer
            foreach (string answerLabel in answersLabels)
            {
                sql.SetParam("@label", answerLabel).Query();
            }

            return lastId;
        }

        /***
        get a single poll by id
        ***/
        public Poll GetPollById(Int32 id)
        {
            // get poll data
            var sqlPoll = (new SqlQuery(
                "SELECT id, title, dateCreation, isMulti, enabled, token, voters"
                + " FROM polls WHERE id=@id;"
            ))
            .SetParam("@id", id)
            .Query();

            // get answers data
            var sqlAnswers = new SqlQuery(
                "SELECT id, label, votes FROM answers WHERE poll_id=@id;"
            )
            .SetParam("@id", id)
            .Query();

            Poll poll = new Poll();

            // data has been returned
            if (sqlPoll.Rows.Count == 1)
            {
                // basic props
                poll.id = (Int32)sqlPoll.Rows[0]["id"];
                poll.title = (string)sqlPoll.Rows[0]["title"];
                poll.dateCreation = (DateTime)sqlPoll.Rows[0]["dateCreation"];
                poll.isMulti = (bool)sqlPoll.Rows[0]["isMulti"];
                poll.enabled = (bool)sqlPoll.Rows[0]["enabled"];
                poll.token = (string)sqlPoll.Rows[0]["token"];
                poll.voters = (Int32)sqlPoll.Rows[0]["voters"];

                // build answers list
                poll.answers = new List<Answer>();
                foreach (DataRow row in sqlAnswers.Rows)
                {
                    poll.answers.Add(new Answer(
                        (Int32)row["id"]
                        , (string)row["label"]
                        , (Int32)row["votes"]
                    ));
                }
            }

            return poll;
        }

        /***
        cast votes for poll
        ***/
        public void CastPollVotes(Int32 pollId, List<Int32> votes) {

            Int32 i = 0;
            List<string> sqlParams = new List<string>();

            // build params list
            i = 0;
            foreach (Int32 vote in votes)
            {
                sqlParams.Add("@vote" + ++i);
            }

            // prepare query
            var sql = new SqlQuery("UPDATE answers SET votes = votes+1 WHERE id IN ("
                + string.Join(",", sqlParams.ToArray())
                + ")");

            // add votes ids to query
            i = 0;
            foreach (Int32 vote in votes)
            {
                sql.SetParam("@vote" + ++i, vote);
            }
            
            // updates votes counts
            sql.Query();

            // update number of voters on this poll
            (new SqlQuery("UPDATE polls SET voters = voters+1 WHERE id = @id"))
                .SetParam("@id", pollId)
                .Query();
        }

        /***
        disable poll by id
        ***/
        public void DisablePollById(Int32 id) {
            (new SqlQuery("UPDATE polls SET enabled = 0 WHERE id = @id"))
                .SetParam("@id", id)
                .Query();
        }
    }
}