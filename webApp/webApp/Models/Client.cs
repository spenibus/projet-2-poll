﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webApp.Models
{
    public class Client
    {

        private string cookiePrefixHasVoted = "poll-hasVoted-";

        public Client()
        {
        }

        // checks if the user can vote in the supplied poll
        public bool CanVote(Int32 pollId)
        {
            HttpCookie ck = HttpContext.Current.Request.Cookies[this.cookiePrefixHasVoted + pollId];
            return (ck != null && ck.Value == "true")
                ? false
                : true;
        }

        // flag the user to know they have voted in the supplied poll
        public Client HasVoted(Int32 pollId)
        {
            // set cookie
            HttpContext.Current.Response.Cookies[this.cookiePrefixHasVoted + pollId].Value = "true";
            HttpContext.Current.Response.Cookies[this.cookiePrefixHasVoted + pollId].Expires = DateTime.Now.AddDays(365);
            // chainable
            return this;
        }
    }
}