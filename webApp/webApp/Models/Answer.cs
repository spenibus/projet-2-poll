﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webApp.Models
{
    public class Answer
    {
        public Int32 id { get; private set; } = 0;
        public string label { get; private set; } = "";
        public Int32 votes { get; private set; } = 0;

        public Answer()
        {
            
        }
        public Answer(Int32 id, string label, Int32 votes)
        {
            this.id = id;
            this.label = label;
            this.votes = votes;
        }

        public void CastVote()
        {
            // increase vote count by 1
        }
    }
}