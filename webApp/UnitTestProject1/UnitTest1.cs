﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics;
using webApp.Models;
using webApp.Toolbox;

/***
any test involving the SqlQuery class currently fails
while the class works in the actual application
this seems to be due to the use of "using"
***/

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        Int32 pollTestId = 140;
        Int32 pollTestVotersValue = 7777;

        /***
        Poll
        ***/
        [TestMethod]
        public void CreateNewPollObject()
        {
            var poll = new Poll();
            Debug.Assert(
                poll.GetType() == typeof(Poll)
                , "Created poll is of type Poll"
            );
        }

        [TestMethod]
        public void NewPollObjectHasIdZero()
        {
            var poll = new Poll();
            Debug.Assert(
                poll.id == 0
                , "New poll has been instantiated with a zero id"
            );
        }

        /*
        [TestMethod]
        public void RetrievePollObjectWithId()
        {
            var poll = (new DAL()).GetPollById(this.pollTestId);
            Debug.Assert(
                poll.GetType() == typeof(Poll)
                , "Created poll is of type Poll"
            );
        }

        [TestMethod]
        public void RetrievedPollObjectHasCorrespondingId()
        {
            var poll = (new DAL()).GetPollById(this.pollTestId);
            Debug.Assert(
                poll.id == this.pollTestId
                , "Retrieved poll has corresponding id"
            );
        }

        [TestMethod]
        public void RetrievedPollObjectHasProperVotersValue()
        {
            var poll = (new DAL()).GetPollById(this.pollTestId);
            Debug.Assert(
                poll.voters == this.pollTestVotersValue
                , "Retrieved poll has the proper value for voters"
            );
        }
        */

        /***
        Answer
        ***/
        [TestMethod]
        public void CreateNewAnswerObject()
        {
            var answer = new Answer();
            Debug.Assert(
                answer.GetType() == typeof(Answer)
                , "Created answer is of type Answer"
            );
        }

        /***
        Client
        ***/
        [TestMethod]
        public void CreateNewClientObject()
        {
            var client = new Client();
            Debug.Assert(
                client.GetType() == typeof(Client)
                , "Created client is of type Client"
            );
        }

        /***
        DAL
        ***/
        [TestMethod]
        public void CreateNewDALObject()
        {
            var dal = new DAL();
            Debug.Assert(
                dal.GetType() == typeof(DAL)
                , "Created dal is of type DAL"
            );
            
        }

        /***
        sql
        ***/
        [TestMethod]
        public void CreateNewSqlQueryObject()
        {
            var sql = new SqlQuery("SELECT 1;");
            Debug.Assert(
                sql.GetType() == typeof(SqlQuery)
                , "Created sql query is of type SqlQuery"
            );
        }

        /*
        [TestMethod]
        public void SqlQueryObjectCanQuery()
        {
            (new SqlQuery("SELECT 1;")).Query();
        }
        */
    }
}
