﻿CREATE TABLE [dbo].[answers] (
    [id]      INT            IDENTITY (1, 1) NOT NULL,
    [label]   NVARCHAR (100) NOT NULL,
    [votes]   INT            NOT NULL DEFAULT 0,
    [poll_id] INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

