﻿CREATE TABLE [dbo].[polls] (
    [id]            INT            IDENTITY (1, 1) NOT NULL,
    [title]         NVARCHAR (100) NOT NULL,
    [dateCreation]  DATETIME       NOT NULL DEFAULT GETDATE(),
    [isMulti]       BIT            NOT NULL DEFAULT 0,
    [enabled]       BIT            NOT NULL DEFAULT 1,
    [deletionToken] NCHAR (64)     NOT NULL,
    [voters]        INT            NOT NULL DEFAULT 0,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

